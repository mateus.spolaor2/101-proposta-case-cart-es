package br.com.cartoes.controllers;

import br.com.cartoes.DTOs.InsereClienteDTO;
import br.com.cartoes.models.Cliente;
import br.com.cartoes.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")

public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente registrarCliente(@RequestBody @Valid InsereClienteDTO clienteDTO) {
        Cliente clienteDB = clienteService.cadastrarCliente(clienteDTO);
        Cliente clienteResponse = new Cliente(clienteDB.getId(), clienteDB.getNome());

        return clienteResponse;
    }


    //Consultar Cliente por ID
    @GetMapping("/{id}")
    public Cliente pesquisarPorId(@PathVariable(name = "id") int id) {
        try {
            Cliente cliente = clienteService.pesquisaClientePorId(id);
            return cliente;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}

