package br.com.cartoes.controllers;

import br.com.cartoes.DTOs.AtualizaCartaoAtivoDTO;
import br.com.cartoes.DTOs.ExibeCartaoDTO;
import br.com.cartoes.DTOs.InsereCartaoRequestDTO;
import br.com.cartoes.DTOs.CartaoResponseDTO;
import br.com.cartoes.models.Cartao;
import br.com.cartoes.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoResponseDTO registraCompra(@RequestBody InsereCartaoRequestDTO insereCartaoRequest){
        try{
            return cartaoService.cadastraCartao(insereCartaoRequest);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{numero}")
    public ExibeCartaoDTO pesquisarPorId(@PathVariable(name = "numero") String numero) {
        try {
            return cartaoService.pesquisaPorNumeroCartao(numero);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping
    public Iterable<Cartao> exibeTodosOsCartoes() {
        return cartaoService.pesquisaCartoes();
    }

    @PatchMapping("/{numero}")
    public CartaoResponseDTO ativaCartao(@PathVariable(name = "numero") String numero,
                                         @RequestBody AtualizaCartaoAtivoDTO atualizaCartaoAtivoDTO){
        try {
            return cartaoService.atualizaPorNumeroCartao(numero, atualizaCartaoAtivoDTO);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
