package br.com.cartoes.models;

import br.com.cartoes.DTOs.PagamentoResponseDTO;

import javax.persistence.*;

@Entity
@Table(name = "pagamentos")
public class Pagamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne
    private Cartao cartao;

    private String descricao;

    private Double valor;

    public Pagamento(){}

    public Pagamento(int id, Cartao cartao, String descricao, double valor) {
        this.id = id;
        this.cartao = cartao;
        this.descricao = descricao;
        this.valor = valor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Cartao getCartao() {
        return cartao;
    }

    public void setCartao(Cartao cartao) {
        this.cartao = cartao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public PagamentoResponseDTO toDTO(){
        PagamentoResponseDTO responseDTO = new PagamentoResponseDTO();

        responseDTO.setId(this.id);
        responseDTO.setDescricao(this.descricao);
        responseDTO.setIdCartao(this.cartao.getId());
        responseDTO.setValor(this.valor);

        return responseDTO;
    }
}
