package br.com.cartoes.services;

import br.com.cartoes.DTOs.InsereClienteDTO;
import br.com.cartoes.models.Cliente;
import br.com.cartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente cadastrarCliente(InsereClienteDTO clienteDTO) {
        Cliente cliente = new Cliente();
        cliente.setNome(clienteDTO.getNome());
        return clienteRepository.save(cliente);
    }

    public Cliente pesquisaClientePorId(int id){
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);

        if(clienteOptional.isPresent()){
            Cliente cliente = clienteOptional.get();
            return cliente;
        }else {
            throw new RuntimeException("O Cliente não foi encontrado!");
        }
    }

}
