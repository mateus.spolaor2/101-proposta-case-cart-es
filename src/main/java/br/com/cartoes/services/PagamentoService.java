package br.com.cartoes.services;

import br.com.cartoes.DTOs.PagamentoResponseDTO;
import br.com.cartoes.DTOs.RealizaPagamentoRequestDTO;
import br.com.cartoes.models.Cartao;
import br.com.cartoes.models.Pagamento;
import br.com.cartoes.repositories.CartaoRepository;
import br.com.cartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    CartaoService cartaoService;

    @Autowired
    PagamentoRepository pagamentoRepository;

    public PagamentoResponseDTO realizaPagamento(RealizaPagamentoRequestDTO pagamentoDTO) {
        Cartao cartaoUtilizado = cartaoService.pesquisaPorId(pagamentoDTO.getIdCartao());

        Pagamento novoPagamento = new Pagamento();
        novoPagamento.setCartao(cartaoUtilizado);
        novoPagamento.setDescricao(pagamentoDTO.getDescricao());
        novoPagamento.setValor(pagamentoDTO.getValor());

        Pagamento pagamentoResponse = pagamentoRepository.save(novoPagamento);
        return pagamentoResponse.toDTO();
    }


    public List<PagamentoResponseDTO> buscaPagamentosPorCartao(String numeroCartao) {
        if (cartaoService.cartaoRepository.existsByNumero(numeroCartao)) {

            Cartao cartaoDB = cartaoService.cartaoRepository.findByNumero(numeroCartao);
            List<PagamentoResponseDTO> pagamentosDoCartao = new ArrayList<>();

            for (Pagamento pagamentoDB : pagamentoRepository.findAllByCartao(cartaoDB)) {
                pagamentosDoCartao.add(pagamentoDB.toDTO());
            }

            return pagamentosDoCartao;

        } else {
            throw new RuntimeException("Cartão não registrado");
        }
    }
}
