package br.com.cartoes.services;

import br.com.cartoes.DTOs.AtualizaCartaoAtivoDTO;
import br.com.cartoes.DTOs.ExibeCartaoDTO;
import br.com.cartoes.DTOs.InsereCartaoRequestDTO;
import br.com.cartoes.DTOs.CartaoResponseDTO;
import br.com.cartoes.models.Cartao;
import br.com.cartoes.models.Cliente;
import br.com.cartoes.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    ClienteService clienteService;

    @Autowired
    CartaoRepository cartaoRepository;

    public CartaoResponseDTO cadastraCartao(InsereCartaoRequestDTO insereCartaoRequest) {
        Cliente cliente = new Cliente();
        Cartao insereCartao = new Cartao();

        cliente = clienteService.pesquisaClientePorId(insereCartaoRequest.getClienteId());

        insereCartao.setAtivo(false);
        insereCartao.setClienteProprietario(cliente);
        insereCartao.setNumero(insereCartaoRequest.getNumero());

        Cartao novoCartao = cartaoRepository.save(insereCartao);

        CartaoResponseDTO response = new CartaoResponseDTO(
                novoCartao.getId(),
                novoCartao.getNumero(),
                novoCartao.getClienteProprietario().getId(),
                novoCartao.isAtivo()
        );

        return response;

    }

    public ExibeCartaoDTO pesquisaPorNumeroCartao(String numeroCartao) {
        try {
            Cartao cartaoPesquisado = cartaoRepository.findByNumero(numeroCartao);

            ExibeCartaoDTO exibeCartaoDTO = new ExibeCartaoDTO();
            exibeCartaoDTO.setId(cartaoPesquisado.getId());
            exibeCartaoDTO.setClienteId(cartaoPesquisado.getClienteProprietario().getId());
            exibeCartaoDTO.setNumero(cartaoPesquisado.getNumero());

            return exibeCartaoDTO;
        } catch (RuntimeException e) {
            throw new RuntimeException("Cartão não encontrado");
        }
    }

    public Cartao pesquisaPorId(int id) {
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);
        if(cartaoOptional.isPresent()){
            Cartao cartao = cartaoOptional.get();
            return cartao;
        } else {
            throw new RuntimeException("Cartão não encontrado");
        }
    }

    public Iterable<Cartao> pesquisaCartoes() {
        return cartaoRepository.findAll();
    }

    public CartaoResponseDTO atualizaPorNumeroCartao(String numero, AtualizaCartaoAtivoDTO atualizaCartaoAtivoDTO) {

        CartaoResponseDTO response = new CartaoResponseDTO();
        Cartao cartaoAtualizado = new Cartao();

        if(cartaoRepository.existsByNumero(numero)){
            Cartao cartaoDB = cartaoRepository.findByNumero(numero);

            cartaoAtualizado = cartaoDB;
            cartaoAtualizado.setAtivo(atualizaCartaoAtivoDTO.isAtivo());
            cartaoRepository.save(cartaoAtualizado);

            response.setAtivo(cartaoAtualizado.isAtivo());
            response.setClienteId(cartaoAtualizado.getClienteProprietario().getId());
            response.setId(cartaoAtualizado.getId());
            response.setNumero(cartaoAtualizado.getNumero());
            return response;
        } else {
            throw new RuntimeException("Registro não existe");
        }
    }
}
